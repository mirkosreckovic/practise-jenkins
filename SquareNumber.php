<?php

class SquareNumber {
	public $number;

	public function getNumber(){
		return $this->number;
	}

	public function setNumber ($num) {
		$this->number = $num;
	}

	public function squareIt(){
		return $this->setNumber($this->getNumber() * $this->getNumber());
	}
}