<?php

require 'SquareNumber.php';

class SquareNumberTest extends PHPUnit\Framework\TestCase {
	public $someVariable;

	public function setUp() {
		$this->someVariable = new SquareNumber();
	}

	public function testing() {
		$this->someVariable->setNumber(10);

		$this->someVariable->squareIt();

		$this->assertEquals(100, $this->someVariable->getNumber());
	}
}